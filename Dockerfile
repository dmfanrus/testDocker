FROM openjdk:8-alpine

COPY build/libs/exlibrus-backend-0.0.1-SNAPSHOT.jar /home/exlibrus-backend-0.0.1-SNAPSHOT.jar

EXPOSE 3006

CMD ["java","-jar","/home/exlibrus-backend-0.0.1-SNAPSHOT.jar"]
